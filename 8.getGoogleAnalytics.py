# -*- coding: utf-8 -*-
"""
Created on Thu Mar  8 09:47:08 2018

@author: sneha
"""
import importlib

commonFunctionClass = importlib.import_module('googleAnalyticsClass')

importlib.reload(commonFunctionClass)

from googleAnalyticsClass import googleVideoAnalytics


googleVideoAnalytics = googleVideoAnalytics()

googleVideoAnalytics.main()

