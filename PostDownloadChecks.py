# -*- coding: utf-8 -*-
"""
Created on Thu Mar  8 12:34:47 2018

@author: sneha
This files checks the final file for errors
"""

import os
import pandas as pd
orig_data_status = pd.read_excel("FinalMergedDataStatus.xlsx",sheet_name=0,skiprows=0)
orig_data_status = orig_data_status.loc[orig_data_status['Download Status']=="Success"]
#orig_data_status['Download Status']=""


allHTMLDF = pd.DataFrame(os.listdir("C:\\Users\\sneha\\Google Drive\\iSpot - 3\\html"),columns=["HTMLFileName"])
allHTMLDF['HTML_FILE_CHECK']="Found"


orig_data_status=pd.merge(orig_data_status,allHTMLDF,on="HTMLFileName",how='left',suffixes=('_left', '_right'))

allVIDEODF = pd.DataFrame(os.listdir("C:\\Users\\sneha\\Google Drive\\iSpot - 3\\Video"),columns=["VideoFileName"])
allVIDEODF['VIDEO_FILE_CHECK']="Found"

orig_data_status=pd.merge(orig_data_status,allVIDEODF,on="VideoFileName",how='left',suffixes=('_left', '_right'))

allDownloadedVideo = pd.read_csv("OutputData/csv/AllVideoList.csv")
allDownloadedVideo['VIDEO_FILE_CHECK']="Found"

orig_data_status=pd.merge(orig_data_status,allDownloadedVideo,on="VideoFileName",how='left',suffixes=('_left', '_right'))

orig_data_status.to_csv("OutputData/csv/AllVideoListCheck.csv")

for idx, row in orig_data_status.iterrows():
    print(row['HTMLFileName'])
    os.path.exists("C:\\Users\\sneha\\Google Drive\\iSpot - 3\\html\\"+row['HTMLFileName'])
    
       