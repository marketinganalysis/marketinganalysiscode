# -*- coding: utf-8 -*-
"""
Created on Sat Feb 17 11:32:57 2018

@author: sneha
"""



import requests as rq

####upload file

import requests
import urllib.parse
import json

headers = {
    'Ocp-Apim-Subscription-Key': 'df839e8afd8e464d921c9e3d71fefe70',
}

form_data = {'file': open('C:\\Users\\sneha\\Desktop\\AdsAnalysis\\data\\video\\17956993_0.mp4', 'rb')}

params ={
    'name': 'videoq.mp4',
    'privacy': 'Private',
    'language': 'English',
}

try:
    url = 'https://videobreakdown.azure-api.net/Breakdowns/Api/Partner/Breakdowns?'
    r = requests.post(url, params=params, files=form_data, headers=headers)
    print(r.url)
    print(json.dumps(r.json(), indent=2))
except Exception as e:
    print("[Errno {0}] {1}".format(e.errno, e.strerror))


### Check the status of file
import re

VideoId = re.sub('"','',r.text)

try:
    url = ''.join(['https://videobreakdown.azure-api.net/Breakdowns/Api/Partner/Breakdowns/',VideoId,'/State?']   )
    rStatus = requests.get(url,headers=headers)
except Exception as e:
    print('[Errno {0}] {1}'.format(e.errno,e.strerror))


rStatusJson = rStatus.json()

rStatusJson['state']

### Once processed, get insight

params = { 'language' : 'English' 
        }

try:
    url =  ''.join(['https://videobreakdown.azure-api.net/Breakdowns/Api/Partner/Breakdowns/',VideoId,'?']   )
    rInsight = requests.get(url, params=params,headers = headers)
    rInsight.json()
except Exception as e:
    print('[Errno {0}] {1}'.format(e.errno,e.strerror))
    
with open('BingJson.json','w') as file:
    json.dump(rInsight.json(),file)
    