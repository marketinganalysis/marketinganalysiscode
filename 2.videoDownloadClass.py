# -*- coding: utf-8 -*-
"""
Created on Thu Feb  8 22:03:47 2018

@author: snehanahu
"""
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException
import pandas as pd
import numpy as np
import time
import sys
import pathlib as Path
import random
import os
import re
import socket
import time    
import requests as rq
from hachoir.parser import createParser
from hachoir.metadata import extractMetadata
from os.path import splitext
import traceback
import importlib

commonClassModule = importlib.import_module('commonFunctionClass')

importlib.reload(commonClassModule)

from commonFunctionClass import commonFunctionClass

class videoDownloadClass(commonFunctionClass):
    def __init__ (self,totalFileToDownload,waitTime):
        self.videoFileName = ''
        self.filePathURL = ''
        self.htmlFileName = ''
        self.duration = ''    
        self.numFileDownload = totalFileToDownload
        self.numberOfFilesDownloaded = 0
        self.statusFile = "OutputData/csv/DailyDownloadStatus.csv"
        self.currentDate = re.sub('[:\s]','_',time.asctime(time.localtime()))
        
    
    def reset (self):
        self.videoFileName = ''
        self.filePathURL = ''
        self.htmlFileName = ''
        self.duration = ''
        self.downloadStatus = ''
        #self.numberOfFilesDownloaded = 0

    def checkForStatusFile(self):
        myStatusFile = Path.Path(self.statusFile)
        if(myStatusFile.is_file()==False):
            print("It seems you are running the file for first time! Execute GenerateStatusFile.py to generate base file!")
        self.statuFileDf = pd.read_csv("OutputData/csv/DailyDownloadStatus.csv")
    
    def downloadVideoFile(self, filePathURL=''):
        self.filePathURL = filePathURL
        self.getFileNameFromURL()
        self.r = rq.get(self.filePathURL, stream = True,timeout=6.0)
        with open(self.videoFileName, 'wb') as fd:
            for chunk in self.r.iter_content(chunk_size=2048):
                fd.write(chunk)
        self.downloadStatus = self.isFileDownloaded()
        #return self.downloadStatus
        """if self.downloadStatus == 'Failed' :
            return False
        else :
            return True"""
                
    def downloadHTMLFile(self,seleniumDriver):
        with open(self.htmlFileName,'wb') as hd:
            html = seleniumDriver.execute_script("return document.getElementsByTagName('html')[0].innerHTML")
            hd.write(html.encode('utf-8'))
                        
    def isFileDownloaded(self):
        parser = createParser(self.videoFileName)
        metadata = extractMetadata(parser)
        try:
            self.duration = metadata.exportPlaintext()[1]
            status = True
        except KeyboardInterrupt:
                        self.writeLog("Shutdown requested...exiting")
                        print("Shutdown requested...exiting")
                        traceback.print_exc()
                        raise Exception("ShutDown_Requested")
                        sys.exit(0)
        except:
            status = False
        return status
    
    def getFileNameFromURL(self):
        self.videoFileName = '_'.join(self.filePathURL.split('/')[-4:])
        self.htmlFileName = splitext(self.videoFileName)[0] + '.html'
        self.videoFileName = 'OutputData/Video/'+self.videoFileName
        self.htmlFileName = 'OutputData/html/'+self.htmlFileName
        
    def openBrowser(self):
        self.browser = webdriver.Chrome('chromedriver')
                
    def main(self):
        self.openBrowser()      
        self.checkForStatusFile()
        self.reset()
        self.createLog(os.path.basename(__file__))
        self.numberOfFilesDownloaded = 0
        for idx, row in self.statuFileDf.loc[pd.isnull(self.statuFileDf['Download Status'])].sort_values('hh_exposures_ad_brand_length',ascending=False).iterrows():
            try:
                self.waitForInternet()
                self.reset()
                self.numberOfFilesDownloaded += 1
                self.listVideoURL = []
                if(self.numFileDownload<0):
                    print("Total file to download is negative")
                    self.writeLog("%s : Fatal Error : Total file to download is negative \n" % time.asctime(time.localtime()))
                    break
                self.status=pd.isnull(row['Download Status'])
                #raise ValueError('Represents a hidden bug, do not catch this')
                if(self.status != True):
                    print("Skipping %s as it has been tried." % row['ad_title'])
                    self.writeLog("{} : Information : Skipping {} as it has been tried. \n".format(time.asctime(time.localtime()),row['ad_title'].encode("utf-8")))
                    continue
                self.browser.get("http://www.google.com")
                print("Downloading %s" % row['ad_title'])
                self.writeLog("{} : Information : Starting Download of {} . \n".format(time.asctime(time.localtime()),row['ad_title'].encode("utf-8")))
                self.searchElement = self.browser.find_element_by_id("lst-ib")
                self.searchElement.clear()
                self.searchElement.send_keys(row['ad_title']+"+ispot")
                self.searchElement.submit()
                #print("reached here-1")
                try:
                    element = WebDriverWait(self.browser, 10).until(
                            EC.presence_of_element_located((By.CLASS_NAME, 'r'))
                            )
                except KeyboardInterrupt:
                        self.writeLog("Shutdown requested...exiting")
                        print("Shutdown requested...exiting")
                        traceback.print_exc()
                        raise Exception("ShutDown_Requested")
                        sys.exit(0)
                except Exception:
                    print("Can not find search result")
                self.results = self.browser.find_elements_by_class_name('r')
                for href in self.results:
                    try:
                        i=href.find_element_by_css_selector('a').get_attribute('href')
                        if 'https://www.ispot.tv/ad' in i:
                            self.listVideoURL.append(i)
                    except NoSuchElementException:
                        self.writeLog("ignoring No such Element Exception")
                        pass
                    except KeyboardInterrupt:
                        self.writeLog("Shutdown requested...exiting")
                        print("Shutdown requested...exiting")
                        traceback.print_exc()
                        raise Exception("ShutDown_Requested")
                    except Exception:
                        e = sys.exc_info()[0]
                        self.writeLog("Error browsing google results")
                        
                #self.listVideoURL = [href.find_element_by_css_selector('a').get_attribute('href') for href in self.results  ]
                #self.listVideoURL = [i for i in self.listVideoURL if 'https://www.ispot.tv/ad' in i]
                self.timeout = 60
                self.listDownloadLink=[]
                self.numberOfFilesDownloaded += 1
                for i in self.listVideoURL:
                    self.waitForInternet()
                    self.browser.get(i)
                    try:
                        self.element_present = EC.presence_of_element_located((By.XPATH , "//video[@class='jw-video jw-reset']"))
                        WebDriverWait(self.browser,self.timeout).until(self.element_present)
                        self.mp4Element = self.browser.find_element_by_xpath("//video[@class='jw-video jw-reset']")
                        self.link = self.mp4Element.get_attribute('src')
                        self.listDownloadLink.append(self.link)
                        self.title = self.browser.find_element_by_xpath("//h1")
                        #print("here")
                        #print("title is "+self.title.text)
                        self.titleText = re.sub('[\W]','',str(self.title.text.encode("utf-8").lower()))
                        self.RowtitleText = re.sub('[\W]','',str(row['ad_title'].encode("utf-8").lower()))
                        self.writeLog(self.titleText)
                        self.writeLog(self.RowtitleText)
                        if(self.titleText==self.RowtitleText):
                            self.writeLog("Downloading")
                            self.downloadVideoFile(self.link)
                        else:
                            self.titleText = self.titleText.replace('tvcommercial','tvspot')
                            if(self.titleText==self.RowtitleText):
                                self.writeLog("Downloading")
                                self.downloadVideoFile(self.link)
                            else:
                                self.downloadStatusFile == False
                                continue
                            
                        if (self.downloadStatus == False):
                            self.downloadStatusFile = "Could not download from any link"
                        else:
                            self.downloadStatusFile = 'Success'
                            print("Downloaded %s" % self.videoFileName)
                            self.writeLog("{} : Information : Downloaded  {} . \n".format(time.asctime(time.localtime()),self.videoFileName))
                            print("{} : Information : Downloaded  {} . \n".format(time.asctime(time.localtime()),self.videoFileName))
                            self.downloadHTMLFile(self.browser)
                            break  
                        
                    except KeyboardInterrupt:
                        self.writeLog("Shutdown requested...exiting")
                        print("Shutdown requested...exiting")
                        traceback.print_exc()
                        raise Exception("ShutDown_Requested")
                    except Exception:
                        self.downloadStatusFile = 'Failed'
                        self.listDownloadLink.append(None)
                        e = sys.exc_info()[0]
                        self.writeLog("Error finding video link on the website: %s \n" % e)
                        self.writeLog("{} : Error {}: Error Finding video link at {} . \n".format(time.asctime(time.localtime()),e,i))
                try:
                    if (self.filePathURL==""):
                        self.downloadStatusFile = "Failed"
                    self.statuFileDf.loc[idx,'Download Status'] = self.downloadStatusFile
                    self.statuFileDf.loc[idx,'VideoUrl'] = self.filePathURL
                    self.statuFileDf.loc[idx,'VideoFileName'] = self.videoFileName
                    self.statuFileDf.loc[idx,'Duration'] = self.duration
                    self.statuFileDf.loc[idx,'HTMLFileName'] = self.htmlFileName
                    os.fsync(self.logFile)
                    self.numFileDownload -= 1
                    if(self.numFileDownload==0 or self.numFileDownload<0):
                        break
                    if(self.numberOfFilesDownloaded>15):
                        print("Saving Intermediate Data")
                        self.writeLog("{} : Information : Saving Data.\n".format(time.asctime(time.localtime())))
                        self.numberOfFilesDownloaded = 1
                        self.statuFileDf.to_csv("OutputData/csv/DailyDownloadStatus.csv",  encoding='utf-8',index = False)
                    self.waitTimeRand = self.waitTime + random.randint(1,10)
                    self.wait(self.waitTimeRand)
                except KeyboardInterrupt:
                        self.writeLog("Shutdown requested...exiting")
                        print("Shutdown requested...exiting")
                        traceback.print_exc()
                        raise Exception("ShutDown_Requested")
                except Exception:
                    e = sys.exc_info()[0]
                    self.writeLog("Error finding video: %s \n" % e)
            except KeyboardInterrupt:
                print("Shutdown requested...exiting")
                traceback.print_exc()
                raise Exception("ShutDown_Requested")
            except Exception:
                e = sys.exc_info()[0]
                print("For loop failure: %s \n" % e)
                self.writeLog("For loop failure: %s \n" % e)
                #self.statuFileDf.loc[idx,'Download Status'] = 'Failed'
            finally:
                self.statuFileDf.to_csv("OutputData/csv/DailyDownloadStatus.csv",  encoding='utf-8',index = False)
            
                
        self.statuFileDf.to_csv("OutputData/csv/DailyDownloadStatus.csv",  encoding='utf-8',index = False)
        self.logFile.close()