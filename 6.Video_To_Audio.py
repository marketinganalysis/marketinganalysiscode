# -*- coding: utf-8 -*-
"""
Created on Sat Jan 20 23:01:00 2018

@author: sneha
This file converts Video to audio and then uploads the audio to Google Cloud
"""

import os
import subprocess as sbp
import importlib
import pandas as pd
import google.cloud.storage as storage

commonFunctionClass = importlib.import_module('googleAnalyticsClass')

importlib.reload(commonFunctionClass)

from googleAnalyticsClass import googleVideoAnalytics


googleVideoAnalytics = googleVideoAnalytics()

for index in os.listdir("Video"):
    print("Converting " + index)
    video_file_name = os.path.join(os.path.dirname("Video\\"),index)
    basefile_name = os.path.splitext(index)[0]+".wav"
    audio_file_name = os.path.join(os.path.dirname("GoogleAudio\\"),basefile_name)
    system_call = "ffmpeg -y -i "+ video_file_name + " -ab 160k -ac 1 -ar 44100 -vn "+audio_file_name
    #split_base_file_name = os.path.splitext(index)[0]+"#"+"%03d.wav"
    #split_audio_file_name = os.path.join(os.path.dirname("GoogleAudio\\"),split_base_file_name)
    #print(system_call)
    sbp.call(system_call)

storage_client = storage.Client()
bucket = storage_client.get_bucket('ads_analysis_audio')
n = 0
for dirpath,_,filenames in os.walk('GoogleAudio'):
    for f in filenames:
        try:
            print(f)
            googleVideoAnalytics.isConnected()
            blob = bucket.get_blob(f)
            if (blob):
                #print(blob)
                print(f+" exists skipping")
            else:
                print(f+" does not exists! Uploading File!")
                blob = bucket.blob(f)
                googleVideoAnalytics.isConnected()
                blob.upload_from_filename(os.path.abspath(os.path.join(dirpath, f)))
                print('File {} uploaded to {}.'.format(os.path.abspath(os.path.join(dirpath, f)),f))
                n = n + 1
                #if n>50:
                    #break
                #break
        except:
            print("Upload Failed for"+ f)

