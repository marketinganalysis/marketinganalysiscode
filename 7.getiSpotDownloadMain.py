"""
This file calls the webscraping module to start the download
"""

import importlib

videoDownloadModule = importlib.import_module('videoDownloadClass')

importlib.reload(videoDownloadModule)

import videoDownloadClass as cs

import pandas as pd

""" 
totalFileToDownload : Number of files we would like to download
WaitTime : Waittime between each download, has to be greater than 0 seconds
"""

classUtility = cs.videoDownloadClass(totalFileToDownload=6000,waitTime=3)

"""
Following file opens the browser and starts the download
""" 
classUtility.main()

classUtility.statuFileDf.to_csv("OutputData/csv/DailyDownloadStatus.csv",  encoding='utf-8',index = False)
