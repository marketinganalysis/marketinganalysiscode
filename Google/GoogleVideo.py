# -*- coding: utf-8 -*-
"""
Created on Fri Feb 16 22:42:35 2018

@author: sneha
"""

import os
import google.cloud.storage

import google.cloud.videointelligence as gvi

storage_client = google.cloud.storage.Client()
bucket_name = 'ads_analysis'

bucket1 = storage_client.get_bucket(bucket_name)

vidoes = bucket1.list_blobs()

videoList = []

for video in vidoes:
    videoList.append(video.name)

"""
bucket = storage_client.list_buckets('MyAdsProject')


for bucket in bucket:
    print(bucket.name)
"""

videoClient = gvi.VideoIntelligenceServiceClient()

features = [gvi.enums.Feature.LABEL_DETECTION,gvi.enums.Feature.EXPLICIT_CONTENT_DETECTION,gvi.enums.Feature.SHOT_CHANGE_DETECTION]

operation = videoClient.annotate_video('gs://ads_analysis/17956993_0.mp4',features= features)

result = operation.result()

from google.protobuf.json_format import MessageToJson
serialized = MessageToJson(result)
from protobuf_to_dict import protobuf_to_dict, dict_to_protobuf

my_message_dict  = protobuf_to_dict(result)

my_message_dict.get('annotation_results')[0].get('shot_label_annotations')[0]

print(my_message_dict )

with open('GoogleJson.json','w') as file:
    json.dump(my_message_dict,file)


import psycopg2    

from psycopg2.extras import Json
    
conn = psycopg2.connect("dbname=AdsAnalysis user=postgres password=ads123..")

cur = conn.cursor()

cur.execute("create table test ( data text);")

cur.execute("insert into test(data) values(%s);",(Json([serialized])))

segment_labels = result.annotation_results[0].shot_label_annotations
for i, segment_label in enumerate(segment_labels):
    print('Video label description: {}'.format(
        segment_label.entity.description))
    for category_entity in segment_label.category_entities:
        print('\tLabel category description: {}'.format(
            category_entity.description))

    for i, segment in enumerate(segment_label.segments):
        start_time = (segment.segment.start_time_offset.seconds +
                      segment.segment.start_time_offset.nanos / 1e9)
        end_time = (segment.segment.end_time_offset.seconds +
                    segment.segment.end_time_offset.nanos / 1e9)
        positions = '{}s to {}s'.format(start_time, end_time)
        confidence = segment.confidence
        print('\tSegment {}: {}'.format(i, positions))
        print('\tConfidence: {}'.format(confidence))
    print('\n')


video_client = videointelligence.VideoIntelligenceServiceClient()
features = [videointelligence.enums.Feature.LABEL_DETECTION]
operation = video_client.annotate_video(path, features=features)
print('\nProcessing video for label annotations:')

result = operation.result(timeout=90)
print('\nFinished processing.')