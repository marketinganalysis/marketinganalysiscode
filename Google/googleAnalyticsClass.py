# -*- coding: utf-8 -*-
"""
Created on Sun Feb 25 20:23:54 2018

@author: sneha
"""
import os
import google.cloud.storage
import google.cloud.videointelligence as gvid
from protobuf_to_dict import protobuf_to_dict, dict_to_protobuf
import pandas as pd
import pickle
import pathlib as paths
import shutil
import importlib

commonFunctionClass = importlib.import_module('commonFunctionClass')

importlib.reload(commonFunctionClass)

from commonFunctionClass import commonFunctionClass

class googleVideoAnalytics(commonFunctionClass):
    """
    This class is created to get the video analytics data from google 
    machine learning API
    """
    def __init__(self):
        self.allAnnotations = []
        if(os.path.exists('saveList.pkl')==True):
            print("Found saveList.pkl file!!! Loading file!!")
            with open('saveList.pkl',rb) as f:
                self.allAnnotations = pickle.load(f)
    def getVideoAnalytics(self, videoFileName,isLabelDetection = False, isSceneChangeDetection = False, isAdultContentDetection = False):
        """
        Shot detection and Adult Content is fetched but is not part of the allAnnotations
        However can accessed using result variable
        
        videoFileName
        -------------
        Google video file name
        
        isLabelDetection
        ----------------
        Set to true if Label Detection is required
        
        isSceneChangeDetection
        ----------------------
        Set to true is Shot Change detection is required
        
        isAdultContentDetection
        -----------------------
        Set to true if adult content is required
        
        Returns a list of all the 
        """
        vdi = gvid.VideoIntelligenceServiceClient()
        self.features = []
        if (isLabelDetection == True):
            self.features.append(gvid.enums.Feature.LABEL_DETECTION)
        
        if (isSceneChangeDetection == True):
            self.features.append(gvid.enums.Feature.SHOT_CHANGE_DETECTION)
                
        if (isAdultContentDetection == True):
            self.features.append(gvid.enums.Feature.EXPLICIT_CONTENT_DETECTION)
            
        operations = vdi.annotate_video(videoFileName,features = self.features)
        
        self.result = operations.result()
        
        self.resultDict  = protobuf_to_dict(self.result)
        
        segment_labels = self.result.annotation_results[0].shot_label_annotations
        for idx, segment_label in enumerate(segment_labels):
            entity_description = segment_label.entity.description
            category_description = [category_entity.description for category_entity in segment_label.category_entities]
            for jidx, segment in enumerate(segment_label.segments):
                start_time = (segment.segment.start_time_offset.seconds +
                              segment.segment.start_time_offset.nanos / 1e9)
                end_time = (segment.segment.end_time_offset.seconds +
                            segment.segment.end_time_offset.nanos / 1e9)
                confidence = segment.confidence
                self.allAnnotations.append([videoFileName,"Label Detections",entity_description,category_description,start_time,end_time,confidence])
                
    def saveIntermediate(self):
        """ 
        saves the list to the pickle file
        
        """
        with open('saveList.pkl','wb') as f:
            pickle.dump(self.allAnnotations)
    
    def main(self):
        self
    
googleVideoAnalytics = googleVideoAnalytics()

googleVideoAnalytics.getVideoAnalytics('gs://ads_analysis/17956993_0.mp4',isLabelDetection=True)

googleVideoAnalytics.allAnnotations