# -*- coding: utf-8 -*-
"""
Created on Wed Mar  7 09:59:48 2018

@author: sneha
This some intermediate analysis file and can be ignored
"""

import pandas as pd

finalTransformed=pd.read_excel("DailyDownloadStatusOrig.xlsx",sheet_name=1)

data = "1 min 59 sec 820 ms".split()


for idx, row in finalTransformed.iterrows():
    try:
        data = row['Duration'].split()
        timeList = [data[x:x+2] for x in range(0,len(data),2)]
        print(timeList)
        time = 0
        for i in timeList:
            if 'min' in i:
                print(i)
                time += int(i[0])*60
            if 'sec' in i:
                time += int(i[0])
            if 'ms' in i:
                time += int(i[0])/1000
        finalTransformed.iloc[idx,9] = time
        print(time)
    except AttributeError:
        pass

finalTransformed.to_excel("FinalMergedDataStatus.xlsx")

test=[data[x:x+2]   for x in range(0,len(data),2)]


'min' in test[1][0]