# -*- coding: utf-8 -*-
"""
Created on Mon Feb 12 15:07:22 2018

@author: sneha
This file generates the initial download status file. 
File generated will be used to webscrape(download the video)
"""
import pandas as pd
import numpy as np
import os
import sys
import pathlib as Path

        
"""Section 1 To be executed only Once/Regenerates the Status file which holds all the downloaded file link """   

""" Read the base file"""

baseAdsFile = pd.read_excel("InputData\\TVision-Exposures-iSpot-AdTitle-Brand-CreativeLength-Primetime-Updated.xls")

""" Remove Local Ads"""
baseAdsFile = baseAdsFile[baseAdsFile['ad_title']!="Local Ad"]

baseAdsFile = baseAdsFile[baseAdsFile['industry_name']!="TV Networks"]

### Removing this steo as we do not need it anymore
"""Get Top Networks"""
#topNetworks = pd.read_excel("Top25Networks.xlsx")

""" Capitalize the brand name"""
baseAdsFile['brand_name'] = baseAdsFile['brand_name'].str.upper()

""" left join ads file data with top networks"""
#baseAdsFileTest = pd.merge(baseAdsFile,topNetworks,on="brand_name",how="left")

""" Filter out the values noy belonging to top networks"""
#baseAdsFileFiltered = baseAdsFileTest[baseAdsFileTest['brand_name_top'].isnull()]

""" Group the filtered out data by ad_title and brand name"""

groupedAds = baseAdsFile.groupby(['ad_title','brand_name'])

""" For each group get maximum creative length and sum of exposures"""
baseAdsFileGrouped = groupedAds.agg({'creative_length': np.max,'hh_exposures_ad_brand_length': np.sum})

""" Next 3 lines are just for exporting ad_title and brand name from the index and add them as column"""    
#baseAdsFileGrouped['ad_title']=baseAdsFileGrouped.index.get_level_values('ad_title')

#baseAdsFileGrouped['brand_name']=baseAdsFileGrouped.index.get_level_values('brand_name')

baseAdsFileGrouped=baseAdsFileGrouped.sort_values('hh_exposures_ad_brand_length',ascending=False)

baseAdsFileGrouped['VideoUrl']=''
baseAdsFileGrouped['VideoFileName']=''
baseAdsFileGrouped['HTMLFileName']=''
baseAdsFileGrouped['Download Status']=''
baseAdsFileGrouped['Duration'] = ''
baseAdsFileGrouped['AllVideoURL']=''
baseAdsFileGrouped['AllError']= ''

myStatusFile = Path.Path("DailyDownloadStatus.xls")
if(myStatusFile.is_file()==False):
    baseAdsFileGrouped.to_csv("OutputData/csv/DailyDownloadStatusOrig.csv",  encoding='utf-8')
else:
    print("This file already exists! Please take backup and delete the file!")