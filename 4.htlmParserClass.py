# -*- coding: utf-8 -*-
"""
Created on Wed Feb 28 11:47:15 2018

@author: sneha
"""

from bs4 import BeautifulSoup 
import pandas as pd
import pickle as pkl
import os
import numpy as np
import importlib
import time
import pyprind
import sys


commonFunctionClass = importlib.import_module('commonFunctionClass')

importlib.reload(commonFunctionClass)

from commonFunctionClass import commonFunctionClass

class htlmParserClass(commonFunctionClass):
    """
    This is the class created to parse the iSpot HTML
    """
    
    def __init__(self):
        """
        initialize the number of file parsed to 0
        """
        self.num_file_parsed = 0
        self.orig_data_status = pd.read_excel("FinalMergedDataStatus.xlsx",sheet_name=0,skiprows=0,usecols=[1,2,7,8])
        self.orig_data_status = self.orig_data_status.loc[self.orig_data_status['Download Status']=="Success"]
        self.orig_data_status['Download Status']=""
        
    
    def reset(self):
        """
        Reset all the file ralated attributes
        """
        self.allList = []
        #self.num_file_parsed = 0
        self.video_details = ''
        self.row_class_details = ''
    
    def readHTML(self,html_file_name):
        """iSpot HTML file reader
        
        filename
        --------
        
        HTML File name
        pass the full path
        
        Returns: File object
        """
        self.html_file_name = html_file_name
        self.file_object = open(self.html_file_name,"r",encoding="utf8")
    def parseHTML(self):
        """ Parse the HTML using Beautiful Soup and returns a chicken Soup(Parsed HTML)"""
        
        self.chicken_soup = BeautifulSoup(self.file_object,"html.parser")
        
    def createEmptyDataFrame(self):
        """ Creates an empty dataframe to store all the hmtl letated data"""
        
        #self.df = {"File_Name":[],"Parent_Feature_Name":[],"Feature_Name":[],"Feature_Value":[]}
        self.df = pd.DataFrame(columns = ["File_Name","Feature_Name","Dummy","Feature_Value","Feature_Href"])
        self.allList = []
        
    def saveListToDf(self):
        """
        It saves the list of all items in the dataframe 
        """
        interimDf = pd.DataFrame([i for i in self.allList if len(i)==5],columns = self.df.columns).drop_duplicates()
        interimDf = interimDf[~interimDf['Feature_Name'].str.contains("unlock")]
        interimDf = interimDf[~interimDf['Feature_Name'].str.contains("Related Advertisers")]
        interimDf = interimDf[~interimDf['Feature_Name'].str.contains("Los Anunciantes Relacionados ")]
        interimDf = interimDf[~interimDf['Feature_Value'].astype(str).str.contains("unlock",na=False)]
        interimDf = interimDf[interimDf['Feature_Value'].notna()]
        interimDf = interimDf[~interimDf['Feature_Value'].astype(str).replace('',np.nan).isnull()]
        interimDf = interimDf[~interimDf['Feature_Value'].astype(str).replace('n/a',np.nan).isnull()]
        interimDf = interimDf[interimDf['Feature_Value']!='None have been identified for this spot']
        interimDf['Feature_Name'] = interimDf['Feature_Name'].str.replace("\s+-\s+(Add)",'')
        self.df = self.df.append(interimDf)
        
        #self.allList  = []
    def readTVadsMetrics(self):
        """
        This function Ad metrics from the HTML file
        """
        try:
            self.video_details=self.chicken_soup.find(id='tvAdMetrics')
            self.getDetails()
        except:
            e = sys.exc_info()[0]
            #self.writeLog("Failed at Metrics " + e)
    
    def readTVadsDetails(self):
        """
        This function extracts video details from the html
        """
        
        try:
            self.video_details=self.chicken_soup.find(class_="container container--page video-details")
            self.getDetails()
        except:
            e = sys.exc_info()[0]
            #self.writeLog("Failed at Details " + e)
        
    def getDetails(self):
        """
        based on the section details passed it does following
            1) extract text/href from element a
            2) extract class to which this element belongs
            3) saves all the information rowwise in allList list.
        """
        try:
            #self.video_details=self.chicken_soup.find(class_="container container--page video-details")
            self.row_class_details = self.video_details.find_all(class_="row")
        except AttributeError:
            pass
        searchString = []
        for idx,i in enumerate(self.row_class_details):
            if idx>0:
                try:
                    searchString = []
                    for x in i.contents:
                        if x != '\n':
                            searchString.append(x.text.replace("\n","").strip())
                            try:
                                searchString.append(','.join([z['href'].replace('#','').replace("\n","").strip() for z in x.find_all('a')]))
                                """for href in x.find_all('a'):
                                    
                                searchString.append(x.find('a')['href'].replace("\n","").strip())
                            """
                            except:
                                searchString.append("")
                    searchString.insert(0,self.html_file_name)
                    if 'ScreenshotsPreviousNext' in searchString:
                        break
                    else:
                        self.allList.append(searchString)
                        self.num_file_parsed += 1
                except:
                    pass
                
    def getDescription(self):
        """
        This function extracts the description (if any) from the html file
        """
        try:
            self.allList.append([self.html_file_name,"description","",self.chicken_soup.find(class_= "description").text.strip('\r\n'),""])
            
        except:
            e = sys.exc_info()[0]
            #self.writeLog("Failed at Description " + e)
    
    def main(self,htmlDir):
        """
        This the main file to extract all the information for the all the html files.
        """
        totalFiles = len(os.listdir(htmlDir))
        print("We are going to process : "+ str(totalFiles) + " files")
        pbar = pyprind.ProgBar(totalFiles)
        self.createLog(os.path.basename(__file__))
        self.createEmptyDataFrame()
        self.num_file_parsed = 0
        for dirpath,_,filenames in os.walk(htmlDir):
            for f in filenames:
                try:
                    self.writeLog("Information: Parsing started for "+os.path.abspath(os.path.join(dirpath, f)))
                    self.reset()
                    self.readHTML(os.path.abspath(os.path.join(dirpath, f)))
                    self.writeLog("\n Information: HTML Read ")
                    self.parseHTML()
                    self.writeLog("\n Information: HTML Parsed ")
                    self.allList.append([self.html_file_name,"title","",self.chicken_soup.find(class_='video-bar__stats-top').find('h1').string,""])
                    self.writeLog("\n Information: Title Read ")
                    self.readTVadsMetrics()
                    self.writeLog("\n Information: Metrics Read ")
                    self.readTVadsDetails()
                    self.writeLog("\n Information: Details Read ")
                    self.getDescription()
                    self.writeLog("\n Information: Description Read ")
                    self.saveListToDf()
                    self.writeLog("\n Information: DF saved Read ")
                    if (self.num_file_parsed > 50):
                        self.saveDFtoCSV("OutputData/csv/interimFileSave.csv",self.df)
                        #print('here')
                        #self.saveDataStructure("OutputData/csv/interimFileSave.pkl",self.df)
                        #print('here2')
                        self.num_file_parsed = 0
                    self.writeLog("\n Information: Parsing complete for "+os.path.abspath(os.path.join(dirpath, f)))
                    #print('here3')
                except:
                    e = sys.exc_info()[0]
                    print(e)
                    self.writeLog("\n Information: Parsing Failed for "+os.path.abspath(os.path.join(dirpath, f)))
                finally:
                    self.saveDFtoCSV("OutputData/csv/interimFileSave.csv",self.df)
                    pbar.update()
                pbar.update()
        try:
            self.df['Feature_Value_New']=np.where(self.df['Feature_Name']=='Advertiser Profiles',self.df['Feature_Href'],self.df['Feature_Value'])
            self.dfp=self.df.pivot(index="File_Name",columns="Feature_Name",values="Feature_Value_New")
            self.saveDFtoCSVWithIndex("OutputData/csv/pivoted_HTML_Parsed_Data.csv",self.dfp)
        except:
            print("Could not save Pivoted dataframe!!! Bailing out, you are on your own. Good luck.")
        

"""                    
video-bar__stats-top

f = open("C:\\Users\\sneha\\Google Drive\\html\\001_000_221_7VVr_360.html","r",encoding='utf-8')

sc = BeautifulSoup(f,"html.parser")

sc.find(class_='video-bar__stats-top').find('h1').string
"""