# -*- coding: utf-8 -*-
"""
Created on Sat Mar  3 20:27:42 2018

@author: sneha
@Description: This class file holds all the common functions
1) Check for internet connection
2) Log creation and Update
3) Saving file
4) sleep(wait)
"""

import time
import sys
import pathlib as Path
import os
import socket
import time    
import pickle as pkl
import re
class commonFunctionClass(object):
    def isConnected(self):
        """
        This file checks if the internet is working or not by pinging google.com
        """
        remoteServer = 'www.google.com'
        try:
            host = socket.gethostbyname(remoteServer)
            s = socket.create_connection((host, 80), 2)
            return True
        except KeyboardInterrupt:
                        self.writeLog("Shutdown requested...exiting")
                        print("Shutdown requested...exiting")
                        raise Exception("ShutDown_Requested")
        except:
            pass
        return False
    
    def waitForInternet(self):
        """
        If internet is not working, this fucntion waits for 60 seconds and rechecks
        """
        while (self.isConnected()==False):
            #waiTime = 60
            print("\n Internet Is not working! We will wait %d seconds and recheck!" % 60)
            self.wait(60)
            
    def wait(self,waitTimeR):
        """
        This is generic wait for seconds.
        waitTimeR
        ---------
        This parameter is the number of seconds that script wants to wait.
        """
        #waiTime = 60
        print("\n Taking Rest now for %s seconds! Do not stare at me!" % waitTimeR)
        self.min = 0
        while (self.min<=waitTimeR):
            print("=",end="")
            time.sleep(1)
            self.min += 1
        print("\n Let us get back to work!")
        
    def createLog(self,fileName):
        """
        This file generates the log file.
        """
        self.currentDate = re.sub('[:\s]','_',time.asctime(time.localtime()))
        self.logFile = open("log/Log_"+fileName+"_"+self.currentDate+".log",'a',1)
        self.logFile.flush()
        os.fsync(self.logFile)
        
    def writeLog(self,text):
        self.logFile.write("{} : {}.\n".format(time.asctime(time.localtime()),text))
        #self.logFile.write(text)
    
            
    def saveDataStructure(self,pkl_file_name,dataStructure):
        with open(pkl_file_name,"w") as f:
            pkl.dump(dataStructure,f)
        f.close()
            
    def saveDFtoCSV(self,fileName, dataFrame):
        dataFrame.to_csv(fileName,mode="w",index=False)
        
    def saveDFtoCSVWithIndex(self,fileName, dataFrame):
        dataFrame.to_csv(fileName,mode="w",index=True)

