# -*- coding: utf-8 -*-
"""
Created on Sun Feb 25 20:23:54 2018

@author: sneha
"""
import os
import google.cloud.storage
import google.cloud.videointelligence as gvid
from protobuf_to_dict import protobuf_to_dict, dict_to_protobuf
import pandas as pd
import pickle
import pathlib as paths
import shutil
import importlib
import sys

commonFunctionClass = importlib.import_module('commonFunctionClass')

importlib.reload(commonFunctionClass)

from commonFunctionClass import commonFunctionClass

class googleVideoAnalytics(commonFunctionClass):
    """
    This class is created to get the video analytics data from google 
    machine learning API
    """
    def __init__(self):
        self.allAnnotations = []
        self.allAnnotationsOneLine = []
        self.numFileProcessed = 0
        self.bucketName = 'ads_analysis'
        self.allAnnotationsDf = pd.DataFrame(columns=['videoFileName',"Label Detections",'entity_description','category_description','start_time','end_time','confidence'])
        if(os.path.exists('OutputData/csv/saveGoogleVideoAnalyticsList.csv')==True):
            print("Found OutputData/csv/saveGoogleVideoAnalyticsList.csv file!!! Loading file!!")
            self.allAnnotationsDf = pd.read_csv('OutputData/csv/saveGoogleVideoAnalyticsList.csv')
            """with open('OutputData/csv/saveGoogleVideoAnalyticsList.pkl','rb') as f:
                self.allAnnotationsDf = pickle.load(f)"""

    def getVideoAnalytics(self, videoFileName,isLabelDetection = True, isSceneChangeDetection = False, isAdultContentDetection = True):
        """
        Shot detection and Adult Content is fetched but is not part of the allAnnotations
        However can accessed using result variable
        
        videoFileName
        -------------
        Google video file name
        
        isLabelDetection
        ----------------
        Set to true if Label Detection is required
        
        isSceneChangeDetection
        ----------------------
        Set to true is Shot Change detection is required
        
        isAdultContentDetection
        -----------------------
        Set to true if adult content is required
        
        Returns a list of all the 
        """
        vdi = gvid.VideoIntelligenceServiceClient()
        self.features = []
        if (isLabelDetection == True):
            self.features.append(gvid.enums.Feature.LABEL_DETECTION)
        
        if (isSceneChangeDetection == True):
            self.features.append(gvid.enums.Feature.SHOT_CHANGE_DETECTION)
                
        if (isAdultContentDetection == True):
            self.features.append(gvid.enums.Feature.EXPLICIT_CONTENT_DETECTION)
            
        operations = vdi.annotate_video(videoFileName,features = self.features)
        
        self.result = operations.result(timeout=180)
        
        self.resultDict  = protobuf_to_dict(self.result)
        print("Fetching Labels ")
        try:
            segment_labels = self.result.annotation_results[0].shot_label_annotations
            self.localOneLiner = []
            self.localOneLiner.append(videoFileName)
            self.localOneLiner.append("Label Detections")
            for idx, segment_label in enumerate(segment_labels):
                entity_description = segment_label.entity.description
                self.localOneLiner.append(entity_description)
                category_description = [category_entity.description for category_entity in segment_label.category_entities]
                self.localOneLiner.append(category_description)
                for jidx, segment in enumerate(segment_label.segments):
                    start_time = (segment.segment.start_time_offset.seconds +
                                  segment.segment.start_time_offset.nanos / 1e9)
                    self.localOneLiner.append(category_description)
                    end_time = (segment.segment.end_time_offset.seconds +
                                segment.segment.end_time_offset.nanos / 1e9)
                    self.localOneLiner.append(end_time)
                    confidence = segment.confidence
                    self.localOneLiner.append(confidence)
                    self.allAnnotations.append([videoFileName,"Label Detections",entity_description,category_description,start_time,end_time,confidence])
        except Exception:
            e = sys.exc_info()[0]
            print("Google Analytics Label failure: %s \n" % e)
            self.writeLog("Google Analytics Label failure: %s \n" % e)
        likely_string = ("Unknown", "Very unlikely", "Unlikely", "Possible",
                     "Likely", "Very likely")
        
        print("Fetching Explicit Content Detection ")
        try:
            pornographyList = self.result.annotation_results[0].explicit_annotation.frames
            for idx, pornCheck in enumerate(pornographyList):
                self.allAnnotations.append([videoFileName,"Explicit Content Detections",likely_string[pornCheck.pornography_likelihood],'Nex two column containg nanos and seconds',pornCheck.time_offset.nanos,pornCheck.time_offset.seconds,''])
        except Exception:
            e = sys.exc_info()[0]
            print("Google Analytics Exlicit Content failure: %s \n" % e)
            self.writeLog("Google Analytics Exlicit Content failure: %s \n" % e)
                
 
    def main(self):
        """
        This main file performs following two operations
        
        1. Check if the file has been processed earlier, if yes skip it.
        
        2. If it has not been processed earlier, attempt getting the video data.
        """
        
        self.createLog(os.path.basename(__file__))
        try:
            self.waitForInternet()
            storage_client = google.cloud.storage.Client()
            bucketList = storage_client.get_bucket(self.bucketName)
            self.videos = bucketList.list_blobs()
            for video in self.videos:
                try:
                    if (self.allAnnotationsDf.iloc[:,0]=='gs://ads_analysis/'+video.name).any():
                        print("Skipping "+ video.name)
                        self.writeLog("Skipping "+ video.name)
                        continue
                except:
                    pass
                try:
                    print("Sit tight : Performing Google Analytics for "+ video.name)
                    self.writeLog("Sit tight : Performing Google Analytics for "+ video.name)
                    self.getVideoAnalytics('gs://ads_analysis/'+video.name,isLabelDetection=True)
                    self.allAnnotationsOneLine.append(self.localOneLiner)
                    self.numFileProcessed += 1
                    if self.numFileProcessed >15:
                        self.numFileProcessed = 0
                        self.saveDataStructure('OutputData/csv/saveGoogleVideoAnalyticsList.pkl',pd.DataFrame(self.allAnnotations,columns=['videoFileName',"Label Detections",'entity_description','category_description','start_time','end_time','confidence']))
                        self.saveDFtoCSV('OutputData/csv/saveGoogleVideoAnalyticsList.csv',pd.DataFrame(self.allAnnotations,columns=['videoFileName',"Label Detections",'entity_description','category_description','start_time','end_time','confidence']))
                        #self.saveDataStructure('OutputData/csv/saveGoogleVideoAnalyticsListOneLine.pkl',pd.DataFrame(self.allAnnotationsOneLine))
                        #self.saveDFtoCSV('OutputData/csv/saveGoogleVideoAnalyticsListOneLine.csv',pd.DataFrame(self.allAnnotationsOneLine))
            
                except Exception:
                    e = sys.exc_info()[0]
                    print("Google Analytics failure: %s \n" % e)
                    self.writeLog("Google Analytics failure: %s \n" % e)
                    
            self.saveDataStructure('OutputData/csv/saveGoogleVideoAnalyticsList.pkl',pd.DataFrame(self.allAnnotations,columns=['videoFileName',"Label Detections",'entity_description','category_description','start_time','end_time','confidence']))
        finally:
            self.saveDataStructure('OutputData/csv/saveGoogleVideoAnalyticsList.pkl',pd.DataFrame(self.allAnnotations,columns=['videoFileName',"Label Detections",'entity_description','category_description','start_time','end_time','confidence']))
            self.saveDFtoCSV('OutputData/csv/saveGoogleVideoAnalyticsList.csv',pd.DataFrame(self.allAnnotations,columns=['videoFileName',"Label Detections",'entity_description','category_description','start_time','end_time','confidence']))
            #self.saveDataStructure('OutputData/csv/saveGoogleVideoAnalyticsListOneLine.pkl',pd.DataFrame(self.allAnnotationsOneLine))
            #self.saveDFtoCSV('OutputData/csv/saveGoogleVideoAnalyticsListOneLine.csv',pd.DataFrame(self.allAnnotationsOneLine))
            


