# -*- coding: utf-8 -*-
"""
Created on Wed Feb 28 11:47:15 2018

@author: sneha
"""

from bs4 import BeautifulSoup 
import pandas as pd
import pickle as pkl


class IspotHTMLParser(object):
    "This is the class created to parse the iSpot HTML"
    
    def readHTML(self,html_file_name):
        """iSpot HTML file reader
        
        filename
        --------
        
        HTML File name
        pass the full path
        
        Returns: File object
        """
        self.html_file_name = html_file_name
        self.file_object = open(self.html_file_name,"r")
    def parseHTML(self):
        """ Parse the HTML using Beautiful Soup and returns a chicken Soup(Parsed HTML)"""
        
        self.chicken_soup = BeautifulSoup(self.file_object,"html.parser")
        
    def createEmptyDataFrame(self):
        """ Creates an empty dataframe to store all the hmtl letated data"""
        
        #self.df = {"File_Name":[],"Parent_Feature_Name":[],"Feature_Name":[],"Feature_Value":[]}
        self.df = pd.DataFrame(columns = ["File_Name","Parent_Feature_Name","Feature_Name","Feature_Value"])
        self.allList = []
        
    def saveListToDf(self):
        self.df = self.df.append(pd.DataFrame(self.allList,columns = self.df.columns).drop_duplicates())
    
    def saveDataStructure(self,pkl_file_name):
        with open(pkl_file_name,"wb") as f:
            pkl.dump(self.df)
    
    def saveDFtoCSV(self):
        self.df.to_csv("analyticsData/html_extracts.csv")
    
    
    def readTVadsMetrics(self):
        try:
            self.ad_metrics_elements = self.chicken_soup.find(id='tvAdMetrics')
            self.row_class =  self.ad_metrics_elements.find_all(class_="row")
        except AttributeError:
            print("TV AD Metrics : Could not find the attribute tvAdMetrics or row class")
            raise AttributeError
            
        for idx, val in enumerate(self.row_class):
            
            try:
                """ Get all the a attributes in the particular row class
                    using find as we want to fetch the a attribute belonging to this row class only"""
                self.all_a = val.find_all('a')
                print(self.all_a)
                for aidx,aval in enumerate(self.all_a):
                    self.interimList = []
                    self.interimList.append(self.html_file_name)
                    """ HTML file has metric name two level above the actual text value
                        Fetching the metric name and appending to the interimList"""
                        
                    self.metric_name = val.find('a').parent.parent.find('div').text
                    self.interimList.append(self.metric_name.replace('\n','').strip())
                    self.interimList.append(aval['href'].replace('\n','').strip())
                    """ Capturing Key error incase href is not found,add a null element"""
                    self.interimList.append(aval.string.replace('\n','').strip())
                    """ Not sure which error can destroy my code, Bad practice but what the heck """
                    #self.interimList.append("")
                    self.allList.append(self.interimList)
            except Exception as e:
                pass

    def readTVadsDetails(self):
        try:
            self.video_details=self.chicken_soup.find(class_="container container--page video-details")
            self.row_class_details = self.video_details.find_all(class_="row")
        except AttributeError:
            raise AttributeError
        for idx, val in enumerate(self.row_class_details):
            if idx == 0:
                continue
            try:
                """ Get all the a attributes in the particular row class
                    using find as we want to fetch the a attribute belonging to this row class only"""
                cont
                self.all_a = val.find('strong').parent.parent.find_all('a')
                print(self.all_a)
                for aidx,aval in enumerate(self.all_a):
                    self.interimList = []
                    self.interimList.append(self.html_file_name)
                    """ HTML file has metric name two level above the actual text value
                        Fetching the metric name and appending to the interimList"""
                        
                    self.metric_name = val.find('strong').string
                    self.interimList.append(self.metric_name.replace('\n','').strip())
                    self.interimList.append(aval['href'].replace('\n','').strip())
                    """ Capturing Key error incase href is not found,add a null element"""
                    self.interimList.append(aval.string.replace('\n','').strip())
                    """ Not sure which error can destroy my code, Bad practice but what the heck """
                    self.allList.append(self.interimList)
            except Exception as e:
                pass
            
IspotHTMLParser = IspotHTMLParser()

IspotHTMLParser.readHTML("html/001_256_040_Akyo_360.html")

IspotHTMLParser.parseHTML()

IspotHTMLParser.createEmptyDataFrame()

IspotHTMLParser.readTVadsMetrics()

IspotHTMLParser.readTVadsDetails()

IspotHTMLParser.saveListToDf()

IspotHTMLParser.html_file_name

IspotHTMLParser.allList

IspotHTMLParser.df.drop_duplicates()

pd.unique(IspotHTMLParser.allList)



IspotHTMLParser.row_class

IspotHTMLParser.all_a

IspotHTMLParser.metric_name



f = open("html/001_256_040_Akyo_360.html",'r')

print(f.read())

soup = BeautifulSoup(f,'html.parser')

d = {'File Name':[],'Parent Feature Name':[],'Feature Name':[],'Feature Value':[]}

df = pd.DataFrame(d)

t=soup.find(id='tvAdMetrics')
k = IspotHTMLParser.row_class_details.find_all(class_="row")

t=k[3].contents
t=t.remove('\n')
for idx,i in enumerate(k):
    if idx>0:
        searchString = [x.text.replace("\n","").strip()  for x in i.contents if x != '\n']
        if 'ScreenshotsPreviousNext' in searchString:
            break
        else:
            searchString

#replace("\n","")
[i for i in k]


allList = []

for iidx,i in enumerate(k):
    try:
        interimDict = []
        d = {'File Name':[],'Parent Feature Name':[],'Feature Name':[],'Feature Value':[]}
        dfInter = pd.DataFrame(d)
        interimDict.append("MyFile")
        print(i.find('a'))
        l = i.find('a').parent.parent.find('div').text
        print(l)
        interimDict.append(l.replace('\n','').strip())
        n = i.find_all('a')
        #print(n)
        for idx,i in enumerate(n):
            interimDict.append(i['href'].replace('\n','').strip())
            print(i['href'].replace('\n','').strip())
                #interimDict["FeatureText"+str(idx)]= i.string
            interimDict.append(i.string.replace('\n','').strip())
            print(i.string.replace('\n','').strip())
            #print(interimDict)
                #df.loc[iidx]= interimDict
            allList.append(interimDict)
    except :
        pass
     
df = df.append(pd.DataFrame(allList[1:],columns=df.columns))


#### Get Video Detailes
t=soup.find_all(class_="container container--page video-details")
k = t[0].find_all(class_="row")


for iidx,i in enumerate(k):
    interimDict = []
    try:
        interimDict.append("MyFile")
        l = i.find('strong').string
        #print(l)
        interimDict.append(l.replace('\n','').strip())
        n = i.find('strong').parent.parent.find_all('a')
        for idx,i in enumerate(n):
            #interimDict["FeatureHref"+str(idx)]= i['href']
            interimDict.append(i['href'].replace('\n','').strip())
            #interimDict["FeatureText"+str(idx)]= i.string
            interimDict.append(i.string.replace('\n','').strip())
            print(interimDict)
            df.loc[iidx]= interimDict
    except:
        pass
        
df.t

IspotHTMLParser = IspotHTMLParser()

IspotHTMLParser.readHTML("html/001_256_040_Akyo_360.html")

IspotHTMLParser.parseHTML()

IspotHTMLParser.createEmptyDataFrame()

IspotHTMLParser.readTVadsMetrics()

IspotHTMLParser.saveListToDf()

IspotHTMLParser.chicken_soup.find(id='tvAdMetrics')

ad_metrics_elements = IspotHTMLParser.chicken_soup.find(id='tvAdMetrics')
            row_class =  ad_metrics_elements.find_all(class_="row")
            
            
pd.DataFrame(IspotHTMLParser.allList),columns  = [IspotHTMLParser.df.columns])