# -*- coding: utf-8 -*-
"""
Created on Wed Feb 28 11:47:15 2018

@author: sneha
"""

from bs4 import BeautifulSoup 
import pandas as pd
import pickle as pkl
import os

class IspotHTMLParser(object):
    "This is the class created to parse the iSpot HTML"
    
    def __init__(self):
        pass
    def reset(self):
        self.allList = []
        
    def readHTML(self,html_file_name):
        """iSpot HTML file reader
        
        filename
        --------
        
        HTML File name
        pass the full path
        
        Returns: File object
        """
        self.html_file_name = html_file_name
        self.file_object = open(self.html_file_name,"r",encoding="utf8")
    def parseHTML(self):
        """ Parse the HTML using Beautiful Soup and returns a chicken Soup(Parsed HTML)"""
        
        self.chicken_soup = BeautifulSoup(self.file_object,"html.parser")
        
    def createEmptyDataFrame(self):
        """ Creates an empty dataframe to store all the hmtl letated data"""
        
        #self.df = {"File_Name":[],"Parent_Feature_Name":[],"Feature_Name":[],"Feature_Value":[]}
        self.df = pd.DataFrame(columns = ["File_Name","Feature_Name","Dummy","Feature_Value","Feature_Href"])
        self.allList = []
        
    def saveListToDf(self):
        self.df = self.df.append(pd.DataFrame(self.allList,columns = self.df.columns).drop_duplicates())
        self.df = self.df[~self.df['Feature_Name'].str.contains("unlock")]
    
    def saveDataStructure(self,pkl_file_name):
        with open(pkl_file_name,"wb") as f:
            pkl.dump(self.df)
    
    def saveDFtoCSV(self):
        self.df.to_csv("analyticsData/html_extracts.csv")
    
    
    def readTVadsMetrics(self):
        self.video_details=self.chicken_soup.find(id='tvAdMetrics')
        self.getDetails()
    
    def readTVadsDetails(self):
        self.video_details=self.chicken_soup.find(class_="container container--page video-details")
        self.getDetails()
        
    def getDetails(self):
        try:
            #self.video_details=self.chicken_soup.find(class_="container container--page video-details")
            self.row_class_details = self.video_details.find_all(class_="row")
        except AttributeError:
            raise AttributeError
        
        for idx,i in enumerate(self.row_class_details):
            if idx>0:
                try:
                    searchString = []
                    for x in i.contents:
                        if x != '\n':
                            searchString.append(x.text.replace("\n","").strip())
                            try:
                                searchString.append(','.join([z['href'].replace("\n","").strip() for z in x.find_all('a')]))
                                """for href in x.find_all('a'):
                                    
                                searchString.append(x.find('a')['href'].replace("\n","").strip())
                            """
                            except:
                                searchString.append("")
                    searchString.insert(0,self.html_file_name)
                    if 'ScreenshotsPreviousNext' in searchString:
                        break
                    else:
                        self.allList.append(searchString)
                except:
                    pass
                
    def getDescription(self):
        self.allList.append([self.html_file_name,"description","",self.chicken_soup.find(class_= "description").text,""])
        
        
IspotHTMLParser = IspotHTMLParser()

IspotHTMLParser.createEmptyDataFrame()
            
for dirpath,_,filenames in os.walk('html'):
       for f in filenames:
           IspotHTMLParser.reset()
           IspotHTMLParser.readHTML(os.path.abspath(os.path.join(dirpath, f)))
           IspotHTMLParser.parseHTML()
           IspotHTMLParser.readTVadsMetrics()
           IspotHTMLParser.readTVadsDetails()
           IspotHTMLParser.getDescription()
           IspotHTMLParser.saveListToDf()

