# -*- coding: utf-8 -*-
"""
Created on Mon Apr 30 13:29:41 2018

@author: sneha
"""

import os
import google.cloud.storage as storage
import google.cloud.videointelligence as gvid
import pandas as pd
import pickle
import pathlib as paths
import shutil
import importlib
import sys
import csv
import pickle
from google.cloud import speech
from google.cloud.speech import enums
from google.cloud.speech import types
import numpy as np
commonFunctionClass = importlib.import_module('googleAnalyticsClass')

importlib.reload(commonFunctionClass)

from googleAnalyticsClass import googleVideoAnalytics


googleVideoAnalytics = googleVideoAnalytics()


googleVideoAnalytics.isConnected()

bucketName = 'ads_analysis_audio'

storage_client = storage.Client()

client = speech.SpeechClient()

bucketList = storage_client.get_bucket(bucketName)


df=pd.read_csv('Data/Google_Cloud_Storage_list_CSV.csv')
words_df=pd.read_csv('Data/Google_Cloud_STT_WORDS.csv')
transcript_df=pd.read_csv('Data/Google_Cloud_STT_TRANSCRIPT.csv')
df_sorted=df.sort_values(by='hh_exposures_ad_brand_length',ascending=False)

#df_sorted=df_sorted.loc[pd.isnull(df_sorted['speech_detection_status'])]
files_process_b4_save = 2

error_threshold = 10

error_count=0

processing_count=0

for i,row in df_sorted.iterrows():
    if pd.isnull(df_sorted.iloc[i,8])==False:
        continue
    try:
        processing_count = processing_count + 1
        audio = types.RecognitionAudio(uri=row['gs_audio'])
        print('Processing '+row['gs_audio'])
        config = types.RecognitionConfig(language_code='en-US',enable_word_time_offsets = True, max_alternatives = 3,
                                         speech_contexts=[speech.types.SpeechContext(phrases=['Act Now','Add to your','Apply today','Be sure to','Book now','Buy and Save','Buy Now','Call today','Check our','Check out','Check this out','Choose your','Click button','Click for more','Click Here','Come see our prices','Compare prices','Contact us','Contact us today','Discover','Do not buy unless','Don’t forget to','Don’t miss','Don’t wait','Download now','Find Items','Find out more','Find savings','Find yours','Follow this','Get a quote','Get Free','Get it here','Get More Info Here','Get the Best','Get your','Give a gift','How to','Hurry','Investigate','Join today','Join us','Learn more','Learn to','Look at','Need more','No obligation to try','Now you can','Order Now','Order Your','Pay Less','Please see','Please view our','Purchase','Read reviews','Register','Request yours today','Research','Respond by','Rush today','Save Big','Save Money','Save on','Save Today','Save up to','Save with','Search for','Search Now','Search our','See deals','See more','See our coupon','See our products','See pricing','Send for','Shop at','Shop low prices','Shop now','Shop online','Shop today','Show price','Sign me up now','Sign up','Start now','Start today','Stock up','Submit','Take a closer look','Take a look at','Take a tour','Tour our','Try it today','View all Products','View features','Visit our','Visit us at','Watch for','You might also try','You might consider','Yours for asking'])])
        googleVideoAnalytics.waitForInternet()
        operation = client.long_running_recognize(config, audio)
        print('Waiting for speech to text operation to complete...')
        response = operation.result(timeout=90)
        alternative = [result.alternatives for result in response.results]
        file_name,extension= os.path.basename(row['gs_audio']).split('.')
        print('Saving the raw file')
        with open('RawSpeechToTextOutputs/'+file_name+'.pickle', 'wb') as handle:
            pickle.dump(response, handle, protocol=pickle.HIGHEST_PROTOCOL)
        n=0
        print('Starting initerpretinng output')
        for alt_text in alternative:
            #print(n)
            all_transcripts=[(file_name,n,n+i,words.transcript,words.confidence)  for i,words in enumerate(alt_text)]
            all_words=[(file_name,n,words.word,words.start_time.seconds,words.start_time.nanos,words.end_time.seconds,words.end_time.nanos)  for words in alt_text[0].words]
            n=n+len(all_transcripts)
            try:
                transcript_df is not None
                transcript_df=transcript_df.append(all_transcripts)
                words_df = words_df.append(all_words)
            except NameError:
                transcript_df = pd.DataFrame(all_transcripts)
                words_df = pd.DataFrame(all_words)
            except AttributeError:
                pass
        df_sorted.iloc[i,8]='success'
        if np.remainder(processing_count,files_process_b4_save)==0:
            googleVideoAnalytics.saveDFtoCSV('Data/Google_Cloud_Storage_list_CSV.csv',df_sorted)
            googleVideoAnalytics.saveDFtoCSV('Data/Google_Cloud_STT_TRANSCRIPT.csv',transcript_df)
            googleVideoAnalytics.saveDFtoCSV('Data/Google_Cloud_STT_WORDS.csv',words_df)
            break
    except Exception as e:
        error_count = error_count + 1
        error_percentage = (error_count/processing_count)*100
        print(repr(e))
        df_sorted.iloc[i,10]=repr(e)
        googleVideoAnalytics.saveDFtoCSV('Data/Google_Cloud_Storage_list_CSV.csv',df_sorted)
        googleVideoAnalytics.saveDFtoCSV('Data/Google_Cloud_STT_TRANSCRIPT.csv',transcript_df)
        googleVideoAnalytics.saveDFtoCSV('Data/Google_Cloud_STT_WORDS.csv',words_df)
        if error_percentage > error_threshold:
            print("Error threshold has reached! Breaking the for loop")
            break
        

"""
for result in response2.results:
    print(result.alternatives[3].transcript)

with open('RawSpeechToTextOutputs/001_489_712_A4Tq_360.pickle','rb') as handle:
    response2=pickle.load(handle)
"""    
